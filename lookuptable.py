# -*- coding: utf-8 -*-
"""
Spyder Editor

Este é um arquivo de script temporário.
"""

snorm = [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]]
smenos = [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]]

for s in range(0,16):
    for N in range(0,16):
        aux = (4 * (N - (s - 1))) 
        if(N>s):         
            print("lut1("+str(s)+","+str(N)+") <= 7D\""+str(aux)+"\";")
            snorm[s].append(aux)
        else :
            print("lut1("+str(s)+","+str(N)+")<= (others => '0');")
        
for s in range(0,16):
    for N in range(0,16):
        aux1 = (4 * (N - s) )
        if(N>s):
            print("lut2("+str(s)+","+str(N)+") <= 7D\""+str(aux1)+"\";")
            smenos[s].append(aux1)
        else :
            print("lut2("+str(s)+","+str(N)+") <= (others => '0');")
            
# -------========= Binary =========-------
 
 
snorm = [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]]
smenos = [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]]

for s in range(0,16):
    for N in range(0,16):
        aux = (4 * (N - (s - 1)))
        if(aux<0):
            val = format(aux,'08b').replace("-","1")
        else:
            val = format(aux,'08b')
        print("lut1("+str(s)+","+str(N)+") <= \""+str(val)+"\";")
        smenos[s].append(aux)
        
for s in range(0,16):
    for N in range(0,16):
        aux1 = (4 * (N - s) )
        if(aux1<0):
            val = format(aux1,'08b').replace("-","1")
        else:
            val = format(aux1,'08b')
        print("lut2("+str(s)+","+str(N)+") <= \""+str(val)+"\";")
        smenos[s].append(aux1)
        
        
