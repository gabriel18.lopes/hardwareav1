library ieee;
use ieee.std_logic_1164.all;
--use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity od_ec_encode is

	generic
	(
		DATA_WIDTH : natural := 16
	);

	port
	(
		clk	    			: in std_logic;
		reset  				: in std_logic;
		s	    				: in std_logic_vector(3 downto 0);
		n	            : in std_logic_vector(3 downto 0);
		fl	 				  : in std_logic_vector((DATA_WIDTH-2) downto 0);
		fh	   			  : in std_logic_vector((DATA_WIDTH-2) downto 0);
		r_inicial	    : in std_logic_vector((DATA_WIDTH) downto 0); -- 16b
		l_inicial	    : in std_logic_vector(((DATA_WIDTH)+8) downto 0); -- 24b

		l_out	: out std_logic_vector(((DATA_WIDTH)) downto 0);
		r_out	: out std_logic_vector(((DATA_WIDTH)) downto 0)
	);

end entity;

architecture sistema of od_ec_encode is

-- look up table
signal look1, look2 : std_logic_vector(6 downto 0):=(others => '0');

-- 9 bits signal
signal fl_shifted, fh_shifted : std_logic_vector(8 downto 0):=(others => '0');

-- r 16b
signal r : std_logic_vector(15 downto 0);

-- l 24b
signal l : std_logic_vector(23 downto 0);

-- u e v 32b
signal u,v : std_logic_vector(31 downto 0);

--__registradores_____________________________
-- register l 24b
signal reg_l : std_logic_vector(23 downto 0);
signal low_ren : std_logic_vector(23 downto 0);
-- register l_shifted
signal reg_r_shifted : std_logic_vector(15 downto 0);
--register r 16b
signal reg_r : std_logic_vector(15 downto 0);
signal rng_renorm : std_logic_vector(15 downto 0);
-- register r_shifted
signal reg_r_shifted : std_logic_vector(15 downto 0);
signal reg_r_shifted_aux : std_logic_vector(31 downto 0);
-- register fl
signal reg_fl : std_logic_vector(14 downto 0);
--__auxiliar signals__________________________

signal sg_fl_shifted_aux, sg_fl_shifted_aux : std_logic_vector ((DATA_WIDTH - 2) downto 0);
signal r_to_renorm,l_to_renorm : std_logic_vector(31 downto 0);

component LUT2d is
	port(
		n			: in 	std_logic_vector(3 downto 0);
		s			: in 	std_logic_vector(3 downto 0);
		result1	: out 	std_logic_vector(6 downto 0);
		result2 : out 	std_logic_vector(6 downto 0)
		);
end component;

begin

-- lookup table (1)
lutt : LUT2d
port map(n => n,s => s,result1 => look1,result2 => look2);

-- shifts (2)
sg_fl_shifted_aux <= to_stdlogicvector(to_bitvector(fl) srl conv_integer(6));
sg_fh_shifted_aux <= to_stdlogicvector(to_bitvector(fh) srl conv_integer(6));

fl_shifted <= sg_fl_shifted_aux(8 downto 0);
fh_shifted <= sg_fh_shifted_aux(8 downto 0);

sg_reset <= reset;

-- salva o primeiro estagio  ------------------------------------------------------
	process (clk)
	begin
		if (rising_edge(clk)) then

			reg_fh_shifted <= fh_shifted;
			reg_fl_shifted <= fl_shifted;
			reg_r_inicial  <= r_inicial;
			reg_l_inicial  <= l_inicial;

			reg_fl <= fl;
			reg_lookupt1 <= look1;
			reg_lookupt2 <= look2;

		end if;
	end process;

--  (3)
process (clk)
begin
	if (rising_edge(clk)) then
		reg_r <= rng_renorm;
	end if;
end process;

r_shifted_aux <= to_stdlogicvector(to_bitvector(reg_r) srl conv_integer(8));
r_shifted <= sg_r_shifted_aux(7 downto 0);

-- v (4)
sg_mult2 <= r_shifted * reg_fh_shifted;
u <= (to_stdlogicvector(to_bitvector(sg_mult1) srl conv_integer(1))) + look1;

-- u (5)
sg_mult1 <= r_shifted * reg_fl_shifted;
v <=	(to_stdlogicvector(to_bitvector(sg_mult2) srl conv_integer(1))) + look2;

-- l_to_renorm(6)
process (clk)
begin
	if (rising_edge(clk)) then
		reg_l <= low_ren;
	end if;
end process;

l_to_renorm <= reg_l+(reg_old_r - reg_u) ;

-- r_to_renorm(7)
process(v,u)
begin
	if(reg_fl < "00000000000000001000000000000000") then
		r_to_renorm <= sg_u - sg_v;
	else
		r_to_renorm <= reg_r - sg_v;
	end if;
end process;

-- registra estagio ------------------------------------------------------
	-- registra esse na borda contraria só para não dar erro
	process (clk)
	begin
		if (falling_edge(clk)) then

			-- it must register old_r just after the third cycle
			-- problem
			if (control_old_r = "11") then
				reg_old_r <= reg_r;
			else
				control_old_r <= control_old_r + 1;
				reg_old_r <= r;
			end if;

			reg_fl2 <= reg_fl;
		end if;
	end process;

	process (clk)
	begin
		if (rising_edge(clk)) then

			if (control_r = '1') then
				reg_r <= sg_new_r;
			else
				reg_r <= r;
				control_r <= '1';
			end if;

			reg_u <= sg_u;
		end if;
	end process;

-- operacao final

process(sg_v,sg_u,reg_r)
begin
	if(reg_fl2 < "00000000000000001000000000000000") then
		sg_new_l <= reg_old_l+(reg_old_r - reg_u) ;
	else
		sg_new_l <= reg_old_l;
	end if;
end process;

process (clk)
begin
	if (falling_edge(clk)) then
		reg_old_l <= reg_l;
	end if;
end process;

process (clk)
begin
	if (rising_edge(clk)) then
		if (control_l = "10") then
			reg_l <= sg_new_l;
		else
			if (control_l = "00") then
				control_l <= "01";
			else
				reg_l <= l;
				control_l <= "10";
			end if;
		end if;
	end if;
end process;

r_out <= reg_r;
l_out <= reg_l;

end sistema;
