
library ieee;
use ieee.std_logic_1164.all;
--use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_signed.all;


entity Teste_ec_encode is

end entity;

architecture T42 of Teste_ec_encode is

	signal clk_sg : std_logic := '0';
	signal resett_sg : std_logic;

	signal sg_s,sg_n : std_logic_vector(3 downto 0);

	signal r_sg,l_sg,fl_sg,fh_sg,final_l,final_r : std_logic_vector(31 downto 0);

	component od_ec_encode is
		generic(DATA_WIDTH : natural := 16);
		port
		(
			clk	    : in std_logic;
			reset   : in std_logic;
			s	    : in std_logic_vector(3 downto 0);
			n	    : in std_logic_vector(3 downto 0);
			r	    : in std_logic_vector(((2*DATA_WIDTH)-1) downto 0);
			l	    : in std_logic_vector(((2*DATA_WIDTH)-1) downto 0);
			fl	    : in std_logic_vector(((2*DATA_WIDTH)-1) downto 0);
			fh	    : in std_logic_vector(((2*DATA_WIDTH)-1) downto 0);
			l_out	: out std_logic_vector(((2*DATA_WIDTH)-1) downto 0);
			r_out	: out std_logic_vector(((2*DATA_WIDTH)-1) downto 0)
		);
	end component;

	begin

		clk_sg <= not clk_sg after 20 ns;

	testador : od_ec_encode
	port map(clk => clk_sg,reset => resett_sg
	,s=>sg_s,n=>sg_n,r=>r_sg,l=>l_sg,fl=>fl_sg
	,fh=>fh_sg,l_out=>final_l,r_out=>final_r);

	process
	begin
		--wait for 30 ns;
		sg_s <= "0011";
		sg_n <= "0111";
		r_sg <= "00000000000000001000000000000000";
		l_sg <= "00000000000000000000000000000000";
		fl_sg <="00000000000000000001000010001111";
		fh_sg <="00000000000000000000000100011100";
		wait for 21 ns;
		sg_s <= "0011";
		sg_n <= "1001";
		r_sg <= "00000000000000001111100001000000";
		l_sg <= "00000000000001101111011011000000";
		fl_sg <= "00000000000000000010010111011010";
		fh_sg <= "00000000000000000000110010000010";
		wait for 40 ns;
		sg_s <= "0011";
		sg_n <= "1001";
		r_sg <= "00000000000000001100001111000000";
		l_sg <= "00000000000111101001011100000000";
		fl_sg <= "00000000000000000010010110101100";
		fh_sg <= "00000000000000000001010000000001";
		wait for 40 ns;
		sg_s <= "0011";
		sg_n <= "1001";
		r_sg <= "00000000000000001101010101101000";
		l_sg <= "00000000000000000000110000011000";
		fl_sg <= "00000000000000000010000000000101";
		fh_sg <= "00000000000000000001011110101110";

	end process;

end T42;
